package com.example.myapplication2;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.myapplication2.net.NoListOPPO;
import com.example.myapplication2.net.NoListXiaomi;
import com.example.myapplication2.permission.Tools;

import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.SaveListener;


public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity---";

    private NoListOPPO NowhiteList = new NoListOPPO();
    private  int PersonNo = 0;
    //一个延时任务
    private static final int DELAY_TIME = 60_000;
    private Handler handler = new Handler();
    Runnable runnable=new Runnable() {
        @Override
        public void run() {
            // TODO Auto-generated method stub
            //要做的事情
            uploadNoWhiteListData();
            handler.postDelayed(this, DELAY_TIME);
            Log.d("MyService","延时任务");
        }
    };
    private void uploadNoWhiteListData(){
        NowhiteList.setName(Build.BRAND.toLowerCase() + PersonNo);
        PersonNo+=1;
        NowhiteList.setAddress("-");
        NowhiteList.save(new SaveListener<String>() {
            @Override
            public void done(String objectId, BmobException e) {
                if(e==null){
                    Log.e(TAG,"添加数据成功，返回objectId为："+objectId);
                }else{
                    Log.e(TAG,"创建数据失败：" + e.getMessage());
                }
            }
        });
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.id_backend_locate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.postDelayed(runnable, 2);//每xx秒执行一次runnable.
                toastShow("开始定时任务");
            }
        });
    }

    private void toastShow(String string){
        Toast.makeText(MainActivity.this,string,Toast.LENGTH_SHORT).show();
    }

}
