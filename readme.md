策略0.白名单、自启动【已添加】
策略1.添加前台服务
策略2.电池处的设置{OPPO应用速冻}
策略3.服务中返回 START_STICKY
策略4.Service  onDestroy 中重启自己
策略5.后台播放无声音乐
策略6.music服务和前台服务相互绑定
策略7.使用全局广播监听锁屏信息（8.0一下有效）

## module说明
- app:做对照组，可忽略
- app_alive:使用的module
- mylibrary：无用

## 策略1：前台服务
包：com.example.myapplication.strategy1 下
启动该服务后，系统会有一个通知，提高任务优先级

## 策略2：电池优化
引导用户做设置。
每个手机会有差异，大致位置在：耗电保护--> 选择允许后台运行
## 策略3：服务中返回
- 在onStartCommand方法中返回 START_STICKY
## 策略4.重启
- ForegroundService：onDestroy方法中，会重启自己和重启音乐服务【音乐服务后面会提到】
- SilentMusicService：onDestroy方法中，会重启自己和重启前台服务
##  策略5.后台播放无声音乐
- 包 com.example.myapplication.strategy3music
## 策略7 广播
- 包：com.example.myapplication.strategy3broadcast
- 注册文件中注册相关的广播
