package com.example.myapplication;

import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.example.myapplication.net.ListOPPO;

import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.SaveListener;


public class Task  {
    private boolean isStarted = false;
    public Task() {
    }

    public void stop(){
        if(!isStarted)return;
        handler.removeCallbacks(runnable);
        isStarted = false;
    }
    public  void start(){
        if(isStarted)return;
        isStarted = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(runnable, 2);
            }
        }).start();

    }
    private  ListOPPO whiteList = new ListOPPO();
    private  int PersonNo = 0;

    //一个延时任务
    private static final int DELAY_TIME = 60_000;
    private Handler handler = new Handler();
    private Runnable runnable=new Runnable() {
        @Override
        public void run() {
            // TODO Auto-generated method stub
            //要做的事情
            uploadWhiteListData();
            handler.postDelayed(this, DELAY_TIME);
            Log.d("MyService","延时任务");
        }
    };



    private void uploadWhiteListData(){
        whiteList.setName(Build.BRAND.toLowerCase() + PersonNo);
        PersonNo+=1;
        whiteList.setAddress("-");
        whiteList.save(new SaveListener<String>() {
            @Override
            public void done(String objectId, BmobException e) {
                if(e==null){
                    //toastShow("添加数据成功，返回objectId为："+objectId);
                }else{
                    //toastShow("创建数据失败：" + e.getMessage());
                }
            }
        });
    }
}
