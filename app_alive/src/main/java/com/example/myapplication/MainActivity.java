package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.view.View;
import android.widget.Toast;

import com.example.myapplication.permission.Tools;
import com.example.myapplication.strategy1.ForegroundService;
import com.example.myapplication.strategy3music.SilentMusicService;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    //前台服务
    private Intent mForegroundService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //开启延时任务
        findViewById(R.id.id_white_list_setting).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //机型判断，跳转到相关的位置
                if(Tools.isHuawei()){
                    goHuaweiSetting();
                }else if(Tools.isXiaomi()){
                    goXiaomiSetting();
                }else if(Tools.isOPPO()){
                    goOPPOSetting();
                }else if(Tools.isVIVO()){
                    goVIVOSetting();
                }else if(Tools.isMeizu()){
                    goMeizuSetting();
                }else if(Tools.isSamsung()){
                    goSamsungSetting();
                }
            }
        });
        //开启无声音乐
        findViewById(R.id.id_silent_music).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 启动服务
                toastShow("开启无声音乐");
                startService(new Intent(MainActivity.this, SilentMusicService.class));

            }
        });        //开启前台服务
        findViewById(R.id.btn_foreground_start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 启动服务
                if (!ForegroundService.serviceIsLive) {
                    // Android 8.0使用startForegroundService在前台启动新服务
                    toastShow("开启前台服务");
                    mForegroundService = new Intent(MainActivity.this, ForegroundService.class);
                    mForegroundService.putExtra("Foreground", "This is a foreground service.");
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        startForegroundService(mForegroundService);
                    } else {
                        startService(mForegroundService);
                    }
                } else {
                    toastShow("前台服务正在运行中...");
                }
            }
        });

        //判断是否在白名单中
        if(!isIgnoringBatteryOptimizations()){
            toastShow("不在白名单中");
            //申请进入白名单
            requestIgnoreBatteryOptimizations();
        }else{
            toastShow("在白名单中");
        }
    }
    /**
     * 跳转到指定应用的首页
     */
    private void showActivity(@NonNull String packageName) {
        Intent intent = getPackageManager().getLaunchIntentForPackage(packageName);
        startActivity(intent);
    }

    /**
     * 跳转到指定应用的指定页面
     */
    private void showActivity(@NonNull String packageName, @NonNull String activityDir) {
        Intent intent = new Intent();
        intent.setComponent(new ComponentName(packageName, activityDir));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void toastShow(String string){
        Toast.makeText(MainActivity.this,string,Toast.LENGTH_SHORT).show();
    }
    /**
     * 判断自己是否在白名单中
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean isIgnoringBatteryOptimizations() {
        boolean isIgnoring = false;
        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        if (powerManager != null) {
            isIgnoring = powerManager.isIgnoringBatteryOptimizations(getPackageName());
        }
        return isIgnoring;
    }

    /**
     * 申请加入白名单
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void requestIgnoreBatteryOptimizations() {
        try {
            @SuppressLint("BatteryLife")
            Intent intent = new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
            intent.setData(Uri.parse("package:" + getPackageName()));
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 华为
     * 跳转到华为管家的启动管理页
     * 操作步骤：应用启动管理 -> 关闭应用开关 -> 打开允许自启动
     */
    private void goHuaweiSetting() {
        try {
            showActivity("com.huawei.systemmanager",
                    "com.huawei.systemmanager.startupmgr.ui.StartupNormalAppListActivity");
        } catch (Exception e) {
            showActivity("com.huawei.systemmanager",
                    "com.huawei.systemmanager.optimize.bootstart.BootStartActivity");
        }
        toastShow("操作步骤：应用启动管理 -> 关闭应用开关 -> 打开允许自启动");
    }

    /**
     * 小米
     * 跳转小米安全中心的自启动管理页面
     * 操作步骤：授权管理 -> 自启动管理 -> 允许应用自启动
     */
    private void goXiaomiSetting() {
        showActivity("com.miui.securitycenter",
                "com.miui.permcenter.autostart.AutoStartManagementActivity");
        toastShow("操作步骤：授权管理 -> 自启动管理 -> 允许应用自启动");
    }

    /**
     * OPPO
     * 跳转 OPPO 手机管家
     * 操作步骤：权限隐私 -> 自启动管理 -> 允许应用自启动
     */
    private void goOPPOSetting() {
        try {
            showActivity("com.coloros.phonemanager");
        } catch (Exception e1) {
            try {
                showActivity("com.oppo.safe");
            } catch (Exception e2) {
                try {
                    showActivity("com.coloros.oppoguardelf");
                } catch (Exception e3) {
                    showActivity("com.coloros.safecenter");
                }
            }
        }
        toastShow("操作步骤：权限隐私 -> 自启动管理 -> 允许应用自启动");
    }

    /**
     * VIVO
     * 跳转 VIVO 手机管家
     * 操作步骤：权限管理 -> 自启动 -> 允许应用自启动
     */
    private void goVIVOSetting() {
        showActivity("com.iqoo.secure");
        toastShow("操作步骤：权限管理 -> 自启动 -> 允许应用自启动");
    }

    /**
     * 魅族
     * 跳转魅族手机管家
     * 操作步骤：权限管理 -> 后台管理 -> 点击应用 -> 允许后台运行
     */
    private void goMeizuSetting() {
        showActivity("com.meizu.safe");
        toastShow("操作步骤：权限管理 -> 后台管理 -> 点击应用 -> 允许后台运行");
    }
    /**
     * 三星
     * 跳转三星手机管家
     * 操作步骤：自动运行应用程序 -> 打开应用开关 -> 电池管理 -> 未监视的应用程序 -> 添加应用
     */
    private void goSamsungSetting() {
        try {
            showActivity("com.samsung.android.sm_cn");
        } catch (Exception e) {
            showActivity("com.samsung.android.sm");
        }
        toastShow("操作步骤：自动运行应用程序 -> 打开应用开关 -> 电池管理 -> 未监视的应用程序 -> 添加应用");
    }


}
