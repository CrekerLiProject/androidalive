package com.example.myapplication.strategy3broadcast;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.example.myapplication.strategy3music.SilentMusicService;
import com.example.myapplication.utils;

import java.util.ArrayList;

import static cn.bmob.v3.Bmob.getApplicationContext;

public class MyReceiver extends BroadcastReceiver {

    private static final String TAG = MyReceiver.class.getSimpleName();


    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if(Intent.ACTION_SCREEN_OFF.equals(action)){
            Log.e(TAG,"MyReceiver ACTION_SCREEN_OFF");
        }else if(Intent.ACTION_SCREEN_ON.equals(action)){
            Log.e(TAG,"MyReceiver ACTION_SCREEN_ON");
        }else if(Intent.ACTION_USER_PRESENT.equals(action)){
            Log.e(TAG,"MyReceiver ACTION_USER_PRESENT");
        }else if(Intent.ACTION_MEDIA_MOUNTED.equals(action)){
            Log.e(TAG,"MyReceiver ACTION_MEDIA_MOUNTED");
        }else if(Intent.ACTION_MEDIA_UNMOUNTED.equals(action)){
            Log.e(TAG,"MyReceiver ACTION_MEDIA_UNMOUNTED");
        }else{
            Log.e(TAG,"else MyReceiver " + action);
        }
        if(!utils.isServiceRunning(context, utils.MUSIC_SERVICE_NAME)){
            // 重启
            context.startService(new Intent(getApplicationContext(), SilentMusicService.class));
        }
        if(!utils.isServiceRunning(context, utils.FORE_SERVICE_NAME)){
            // 重启
            context.startService(new Intent(getApplicationContext(), SilentMusicService.class));
        }

        // an Intent broadcast.
        throw new UnsupportedOperationException("Not yet implemented");
    }

}
